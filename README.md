## Test Plan ##

The following is not a formal Test Plan. What I provide below is closer to a testing mind map using exploratory testing approach that lists and links the areas of focus.

1. User Input
    * Search by id only
    * Search by other fields - not possible
    * Correct input
    * Incorrect input
        * Expected format
        * Unexpected format
2. Data returned when interacting with UI only
    * All displayed field format validation
    * Validation and presenting errors to the User
    * Manipulating data from the UI
    * Searching and filtering
    * Pagination and result number calculation
    * Incorrect input handling
        * Server Errors
            * Console Message (E2E)
            * Error code
            * Response Data
3. Data returned when interacting with API directly
    * All json field format validation
    * Incorrect input handling
        * Server Errors
            * Error code
            * Error message
            * Response Data
    * API Fuzzing
        * Fuzzing with request headers
            * Cookie
            * Encoding
            * Charsets
        * Fuzzing with request endpoint
            * Other possible versions (/v1)
            * Other possible arguments (&first=John)
            * Malicious script injection (no experience with that one)
        * Fuzzing with request method
            * GET, PUT, POST etc.
        * Fuzzing with authentication
            * SessionIDs
            * Data Access by SessionID
            * Session Timeout

### General note ###
The tests are not covering all the test scenarios mentioned above, and sometimes use a simplified approach. I did that to save some time on helper function development, and rather focus on logic. I tried add appropriate comments in such places in the code.

## E2E Tests ##

### Set up ###

1. Open the solution in E2E folder with Visual Studio 2015 (requires F# Visual installed, and .Net 4.5.2)
2. Build the solution
3. Run the solution in any mode or via command line with argument `e2e`
```
C:/theRepo> SwipeJobs.exe e2e
```
**Note:** If you decide to run the tests from Visual Studio in Debug mode, the execution will break on each test failure by default
4. The tests are set up to run on chrome only

### E2E Test Report ###

The E2E test will produce a test report that will be saved in `c:/logs/` folder. An example of the report has been placed in the repo [test report](test_report_20170504-092003.html)

The application exit code is the amount of failed tests to be easily applied into a CI framework
**Note:** Some of the failures take 10 seconds to discover by design, as the framework is waiting that time for the DOM to update before failing the test. The timeout is configurable and can be disabled.

## API tests ##

### Set up

Running the tests require python 2.7, and can be called with the following command

```
[$path]/API_TEST> python -m unittest discover -s [$path]/API_TEST -p test.py -t [$path]/API_TEST
```

Or with built-in IDE integration (e.g. PyCharm)

The API tests are written in python with the use of the `unittest` framework. They focus on testing the `getWorkController` endpoint, even though I do realize the main application calls also `restBestRankedJobsByWorker`, but the tests would be very similar, so I skipped that part and hard-coded the values for `getWorkController` to reduce the amount of code, and time, as I wanted to fit in the 1 week timeframe to return the results. Exit code is handled by `unittest` framework and returns `-1` on failure.