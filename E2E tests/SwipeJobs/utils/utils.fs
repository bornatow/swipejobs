module Utils

open canopy

let rnd = System.Random()

let randomStringNoDigits = 
    let chars = "ABCDEFGHIJKLMNOPQRSTUVWUXYZqwertyuiopasdfghjklzxcvbnm@#$&()-_+{}[]"
    let charsLen = chars.Length

    fun len -> 
        let randomChars = [|for i in 0..len -> chars.[rnd.Next(charsLen)]|]
        new System.String(randomChars)

let isDisplayed sel =
    try (unreliableElement sel).Displayed
    with _ -> false

let waitUntilDisplayed sel =
    waitFor (fun _ -> isDisplayed sel)