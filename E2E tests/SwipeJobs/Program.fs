﻿module Program

open canopy
open canopy.reporters
open System
open System.IO


[<EntryPoint>]
let main args =

    let testArg = args.[0].ToLower()

    // environment
    let path = System.AppDomain.CurrentDomain.BaseDirectory

    // configure html reporter
    let appendix = DateTime.Now.ToString("yyyyMMdd-HHmmss")

    reporter <- new LiveHtmlReporter(Chrome, path) :> IReporter
    let liveHtmlReporter = reporter :?> LiveHtmlReporter
    liveHtmlReporter.reportPath <- Some ("C:\\logs\\test_report_" + appendix)

    // test setup
    showInfoDiv <- false // added to remove the message inserted by canopy inside the browser
    start chrome
    pin FullScreen

    match testArg with
    | "e2e" -> SwipeMain.runner()
    | _ -> Console.WriteLine("Incorrect test argument");

    try run()
    finally quit()
    canopy.runner.failedCount // program exit code is the number of failed tests