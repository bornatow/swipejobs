module SwipeMain

open canopy
open SwipeMainPageFunctions
open SwipeMainPageObjects
open Utils

let runner _ =
    
    context "Valid Worker search"
    once (fun _ -> 
        url _mainUrl
    )

    "Search for a worker using valid ID should display matching jobs" &&& fun _ ->
        searchForWorkerAndWaitForJobTable Valid
    
    "Worker Info should display in correct format" &&& fun _ ->
        verifyWorkerInfoDataFormat()

    "All worker info fields should be read only" &&& fun _ ->
        assertAllInfoFieldsAreReadOnly()

    "Filtering should display only the matching results" &&& fun _ -> 
    // test performs 4 times a random valid search and asserts the results
        for i in 1..4 do verifyJobFiltering Valid
    
    "Filtering out all the results should display a blank data table" &&& fun _ ->
        verifyJobFiltering Invalid

    "Tests for table sorting, pagination and Number of displayed results per page go here" &&& todo


    context "Searching for worker outside of the ID range"
    once (fun _ ->
        url _mainUrl
    )

    "Search for a worker from outside of ID range should display error message" &&& fun _ -> 
        searchForWorker OutOfRange
        _errorMessage == workerNotFoundErrorMessage // assertion

    "All worker info fields should be blank" &&& fun _ ->
        assertAllInfoFieldsAreBlank()

    "The job data table should display no data message" &&& fun _ ->
        assertDataTableDisplaysNoResultsMessage()
    

    context "Invalid data search"
    before (fun _ -> // before each
        url _mainUrl
    )

    "Search for a worker using invalid ID should display validation message" &&& fun _ ->
        searchForWorker Invalid
        _validationMessage == inputValidationMessage

    "All worker info fields should be blank" &&& fun _ ->
        assertAllInfoFieldsAreBlank()

    "The job data table should display no data message" &&& fun _ ->
        assertDataTableDisplaysNoResults()
        _emptyDataTable == noDataAvailableMessage

    "Empty search should display empty search validation message" &&& fun _ ->
        searchForWorker Empty
        _validationMessage == inputCannotBeBlankMessage

    "All worker info fields should be blank" &&& fun _ ->
        assertAllInfoFieldsAreBlank()

    "The job data table should display no data message" &&& fun _ ->
        assertDataTableDisplaysNoResultsMessage()

    
    context "Consecutive searches"
    // In case where whole tests are being reused like in here, they could be extracted to another module as functions
    once(fun _ -> 
        url _mainUrl
    )

    "Search for a worker using valid ID should display matching jobs" &&& fun _ ->
        searchForWorkerAndWaitForJobTable Valid

    "Search for a worker from outside of ID range should display error message" &&& fun _ -> 
        searchForWorker OutOfRange
        _errorMessage == workerNotFoundErrorMessage // assertion

    "All worker info fields should be blank" &&& fun _ ->
        assertAllInfoFieldsAreBlank()

    "The job data table should display no data message" &&& fun _ ->
        assertDataTableDisplaysNoResultsMessage()
