module SwipeMainPageObjects

open Utils

// selector builders
let private anyElementSelector tag ngModel =
    sprintf "%s[ng-model='%s']" tag ngModel

let private inputFieldSelector ngModel =
    anyElementSelector "input" ngModel

let private buttonSelector ngModel =
    anyElementSelector "button" ngModel

// page object selectors
let _mainUrl = "http://qa-test.swipejobs.com/swipeJobsAPI"

// worker search
let _workerIdSearchField = inputFieldSelector "vm.workerId"
let _searchButton = buttonSelector "vm.buttonSearchWorker"

// worker info
let _allworkerInfoFields = "input[ng-model^='vm.workerInfo']"
let _workerNameField = inputFieldSelector "vm.workerInfoName"
let _workerAgeField = inputFieldSelector "vm.workerInfoAge"
let _workerEmailField = inputFieldSelector "vm.workerInfoEmail"
let _workerPhoneField = inputFieldSelector "vm.workerInfoPhone"
let _workerRatingField = inputFieldSelector "vm.workerInfoRating"
let _workerIsActiveField = inputFieldSelector "vm.workerInfoIsActive"

// worker history
let _selectDropdown = "select[name^='DataTables_Table']"
let _resultSearchField = ".dataTables_filter input"
let _resultHeaders = ".dataTable th"
let _resultRows = ".dataTable tbody tr"
let _resultCells= ".dataTable tbody tr td"

let _tableInfo = ".dataTables_info"
let _currentPage = ".paginate_button.current"
let _paginateButtonNext = ".paginate_button.next"
let _paginateButtonPrevious = ".paginate_button.previous"

let _emptyDataTable = ".dataTables_empty"

// the following objects does not exist in the page, but they should have been implemented
let _errorMessage = "#errorMessage"
let _validationMessage = "#validationMessage"

// test data
let maxWorkerId = 50
let workerNotFoundErrorMessage = "There is no worker with that ID in the database. Please refine your search."
let inputValidationMessage = "Incorrect input. Please enter digits only."
let inputCannotBeBlankMessage = "Input cannot be blank."
let noDataAvailableMessage = "No data available in table"
let noMatchingRecordsMessage = "No matching records found"

let nameRegex = "[^0-9]+"
let ageRegex = "^[0-9]{2}$"
let emailRegex = "^[a-zA-Z0-9.!#$%&�*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"
let phoneRegex = "^\+1 \([0-9]{3}\) [0-9]{3}-[0-9]{4}$"
let ratingRegex = "^[0-9]{1,2}$"
let isActiveRegex = "^(true|false)$"
