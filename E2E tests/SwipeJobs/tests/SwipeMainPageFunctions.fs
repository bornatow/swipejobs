﻿module SwipeMainPageFunctions

open canopy
open OpenQA.Selenium
open SwipeMainPageObjects
open Utils

type SearchType = | Valid | OutOfRange | Invalid | Empty

let getValidWorkerId _ = rnd.Next(maxWorkerId) |> string
let getWorkerIdOutOfRange _ = rnd.Next(maxWorkerId, maxWorkerId + 20) |> string
let getInvalidWorkerId _ = randomStringNoDigits 5

let getWorkerIdBy searchType =
    match searchType with
    | Valid      -> getValidWorkerId()
    | OutOfRange -> getWorkerIdOutOfRange()
    | Invalid    -> getInvalidWorkerId()
    | Empty      -> ""

let searchForWorker searchType =
    _workerIdSearchField << getWorkerIdBy searchType
    click _searchButton

let searchForWorkerAndWaitForJobTable searchType =
    searchForWorker searchType
    waitUntilDisplayed _currentPage

let verifyWorkerInfoDataFormat _ =
    _workerNameField =~ nameRegex
    _workerAgeField =~ ageRegex
    _workerEmailField =~ emailRegex
    _workerPhoneField =~ phoneRegex
    _workerRatingField =~ ratingRegex
    _workerIsActiveField =~ isActiveRegex

let assertDataTableDisplaysNoResults _ =
    displayed _emptyDataTable
    notDisplayed _currentPage

let assertDataTableDisplaysNoResultsMessage _ =
    assertDataTableDisplaysNoResults()
    _emptyDataTable == noDataAvailableMessage

let getRandomResultCellText _ =
    elements _resultCells
    |> List.map (fun cell -> cell.Text)
    |> fun textList -> textList.[rnd.Next(textList.Length)] // select random list element

let generateResultFilteringString searchType =
    match searchType with
    | Valid -> getRandomResultCellText()
    | _ ->  getRandomResultCellText() + randomStringNoDigits 5

let verifyResultFiltering searchType searchString =
    match searchType with
    | Valid ->  elements _resultRows
                |> List.iter (fun row -> containsInsensitive searchString row.Text)
    | _     ->  assertDataTableDisplaysNoResults()
                _emptyDataTable == noMatchingRecordsMessage

let verifyJobFiltering searchType =
    // This should be part of the DDT approach, here is a heuristic version
    // Search for a random result that is displayed on the page
    let searchString = generateResultFilteringString searchType
    _resultSearchField << searchString

    // Assert that only rows with the searched text remained displayed or the table is empty
    // This verification has a performance related risk that on other machines then mine, 
    // the search result would be calculated with a delay, and the test would start verifying a table with no filter applied yet
    verifyResultFiltering searchType searchString

    // Clear the search
    clear _resultSearchField
    (element _resultSearchField).SendKeys(Keys.Backspace) // just clearing does not reset the filter

let assertAllInfoFieldsAreReadOnly _ =
    elements _allworkerInfoFields
    |> List.iter (fun infoField ->
                        if infoField.GetAttribute("readonly") <> "true" then // error with custom message
                            "Worker Info field is not read-only!" |> CanopyReadOnlyException |> raise)

let assertAllInfoFieldsAreBlank _ =
    elements _allworkerInfoFields
    |> List.iter (fun infoField ->
                        let fieldText = read infoField
                        if fieldText <> "" then
                            sprintf "Expected a blank field, but got <b>%s</b>" fieldText |> CanopyEqualityFailedException |> raise)
