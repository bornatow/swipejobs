import gzip, json, re, unittest

from urllib2 import urlopen, Request, HTTPError
from StringIO import StringIO


api_url = "http://qa-test.swipejobs.com/"
request_header = {
                    "Accept": "application/json, text/javascript, */*; q=0.01",
                    "Accept-Encoding": "gzip, deflate, sdch",
                    "Accept-Language": "en-GB,en;q=0.8,en-US;q=0.6,pl;q=0.4",
                    "Cache-Control": "no-cache",
                    "Content-Type": "application/json; charset=utf-8",
                    "Host": "qa-test.swipejobs.com",
                    "Pragma": "no-cache",
                    "Proxy-Connection": "keep-alive",
                    "Referer": "http://qa-test.swipejobs.com/swipeJobsAPI",
                    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
                    "X-Requested-With": "XMLHttpRequest"
}


class RequestWithMethod(Request):
    """Needed to overwrite Request class to add http method support"""
    def __init__(self, *args, **kwargs):
        self._method = kwargs.pop('method', None)
        Request.__init__(self, *args, **kwargs)

    def get_method(self):
        return self._method if self._method else super(RequestWithMethod, self).get_method()


class TheAPI(object):
    def get_response_headers(self, url):
        return urlopen(url).info()

    def get_cookie(self):
        """ Can throw ValueError when Set-Cookie value is not correct"""
        info = self.get_response_headers(api_url)
        full_cookie = info.get("Set-Cookie")
        cookie = full_cookie[:full_cookie.index(";")]
        return cookie

    def replace_cookie_in_header(self, cookie):
        request_header["Cookie"] = cookie

    def make_url_with_parameter(self, param_name, param_value):
        return api_url + "getWorkController?%s=%s" % (param_name, param_value)

    def prepare_request_with_method(self, user_id, cookie=None, http_method='POST', param_name="userId"):
        url = self.make_url_with_parameter(param_name, user_id)
        if cookie is None: cookie = self.get_cookie()
        self.replace_cookie_in_header(cookie)
        return RequestWithMethod(url, headers=request_header, method=http_method)

    def prepare_request_to_server(self, user_id, cookie=None):
        return self.prepare_request_with_method(user_id, cookie)

    def get_server_response_code(self, request):
        """Error code raises errors in urllib2"""
        try:
            return urlopen(request).getcode()
        except HTTPError as e:
            error_code = int(re.search("[0-9]+", str(e)).group(0))
            return error_code

    def get_server_response_headers(self, request):
        return urlopen(request).info()

    def get_server_response_json(self, user_id):
        request = self.prepare_request_to_server(user_id)
        buf = StringIO(urlopen(request).read())
        zipped_response = gzip.GzipFile(fileobj=buf)
        response = zipped_response.read()
        return json.loads(response)


class TestAPI(unittest.TestCase):
    def setUp(self):
        self.api = TheAPI()

    def test_contract_with_main_page(self):
        user_id = "3"
        json_response = self.api.get_server_response_json(user_id)
        self.assertIs(json_response["userId"], int(user_id))
        self.assertIsInstance(json_response["name"]["last"], unicode)
        self.assertIsInstance(json_response["name"]["first"], unicode)
        self.assertNotRegexpMatches(json_response["name"]["last"], r"[0-9]")
        self.assertNotRegexpMatches(json_response["name"]["first"], r"[0-9]")
        self.assertIsInstance(json_response["age"], int)
        self.assertLess(json_response["age"], 80)
        self.assertGreaterEqual(json_response["age"], 16)
        self.assertIsInstance(json_response["phone"], unicode)
        self.assertRegexpMatches(json_response["phone"], r"^\+1 \([0-9]{3}\) [0-9]{3}-[0-9]{4}$")
        self.assertRegexpMatches(json_response["email"], r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
        self.assertIsInstance(json_response["active"], bool)

    # Number of active sessions
    def test_number_of_valid_cookies(self):
        """Should be based on API documentation on max supported sessions per host"""
        number_of_cookies = 5
        user_id = "5"
        cookies = []
        for i in range(number_of_cookies):
            cookies.append(self.api.get_cookie())
        for cookie in reversed(cookies):
            request = self.api.prepare_request_to_server(user_id, cookie)
            server_response_code = self.api.get_server_response_code(request)
            self.assertEqual(server_response_code, 200)
            # should return 401 sfter reaching the limit

    # HTTP Methods response codes only - the tests should also validate the messages
    def test_options_method_should_returns_200(self):
        """Need to add tests for the response body. The tests would e.g. send invalid inputs and/or options,
        validating that 5xx error is not raised"""
        user_id = "1"
        request = self.api.prepare_request_with_method(user_id, http_method='OPTIONS')
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 200)

    def test_connect_method_should_returns_400(self):
        user_id = "2"
        request = self.api.prepare_request_with_method(user_id, http_method='CONNECT')
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 400)

    def test_get_method_should_returns_405(self):
        user_id = "3"
        request = self.api.prepare_request_with_method(user_id, http_method='GET')
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 405)

    def test_put_method_should_returns_405(self):
        user_id = "4"
        request = self.api.prepare_request_with_method(user_id, http_method='PUT')
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 405)

    def test_delete_method_should_returns_405(self):
        user_id = "5"
        request = self.api.prepare_request_with_method(user_id, http_method='DELETE')
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 405)

    def test_trace_method_should_returns_405(self):
        user_id = "6"
        request = self.api.prepare_request_with_method(user_id, http_method='TRACE')
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 405)

    def test_head_method_should_returns_405(self):
        user_id = "7"
        request = self.api.prepare_request_with_method(user_id, http_method='HEAD')
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 405)

    # session id validation
    def test_incorrect_cookie_returns_401(self):
        user_id = "8"
        cookie = "Don't even have to come up with hex"
        request = self.api.prepare_request_with_method(user_id, cookie)
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 401)

    # userId range
    def test_userId_out_of_range_returns_404(self):
        user_id = "9"
        request = self.api.prepare_request_to_server(user_id)
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 404)

    # invalid userId
    def test_invalid_userId_returns_400(self):
        """An actuall test would generate random data and interate hundred of times to cause an unexpected server response.
        The 5xx errors can provide data like internal server stacktrace, and error messages 
        that could potentially give out information for the next steps of hack"""
        user_id = "Nabuhodozor"
        request = self.api.prepare_request_to_server(user_id)
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 400)

    def test_empty_userId_returns_400(self):
        user_id = ""
        request = self.api.prepare_request_to_server(user_id)
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 400)

    # invalid request reflection
    def test_invalid_strings_are_not_reflected_back_to_client(self):
        """This test is not sufficient"""
        user_id = "5&allyourbasearebelongtous=true"
        request = self.api.prepare_request_to_server(user_id)
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 400)

    def test_unsupported_parameter_request(self):
        """The data used here is just an example, more should be tested with DDT approach.
        This sentence is actually true for most of the other tests as well"""
        param_name = "active"
        param_value = "true"
        request = self.api.prepare_request_with_method(param_value, param_name=param_name)
        server_response_code = self.api.get_server_response_code(request)
        self.assertEqual(server_response_code, 400)

# Request header manipulation tests here